/**
 * Proxy Mapper
 */

/* TODO: Validate if service is active, otherwise start it. */

var proxyMiddleware = require('http-proxy-middleware');

// create the proxy
module.exports = proxyMiddleware('/', {
  target: 'http://127.0.0.1:3001'
});